/**
 * Created by dzambuto on 27/09/15.
 */

var debug = require('debug')('micro:task');

var _ = require('underscore')
  , util = require('util')
  , net = require('net')
  , BufferStream = require('bufferstream')
  , EventEmitter = require('events').EventEmitter;

var noop = function () {};

function Task (code, options) {
  options = options || {};

  EventEmitter.call(this);

  _.defaults(this, options, {
    factory: null,
    socket: '/tmp/worker.sock',
    delay: 500,
    separator: '\u0000\u0000'
  });

  this.code = code;
  this.hasData = false;
}

util.inherits(Task, EventEmitter);

Task.prototype.run = function(args, fn) {
  var task = this;

  if (_.isFunction(args)) {
    fn = args;
    args = {};
  }

  this.callback = _.isFunction(fn)
    ? fn
    : noop;

  this.factory.ready(function () {
    var stream, connection;

    connection = net.createConnection(task.socket, function () {
      var req = {
        code: task.code,
        args: args
      };

      connection.write(JSON.stringify(req) + task.separator);
    });

    connection.on('error', function (err) {
      if (!task.hasData) {
        connection.end();
        task.hasCompleted(err);
      }
    });

    connection.on('close', function () {
      if (!task.hasData) {
        connection.end();
        task.hasCompleted(new Error('Unexpected error'));
      }
    });

    stream = new BufferStream({ size:'flexible' });

    stream.split(task.separator, function (data) {
      connection.end();
      task.hasCompleted(null, data);
    });

    connection.on('data', function(data) {
      task.hasData = true;
      stream.write(data);
    });
  });
};

Task.prototype.hasCompleted = function (err, res) {
  var error
    , args = {};

  if (err) {
    return this.callback(err);
  }

  try {
    if (!_.isEmpty(res)) {
      res = JSON.parse(res);

      if (!_.isUndefined(res) && res.error) {
        error = new Error(res.error.message);
        error.stack = res.error.stack;
      }

      args = _.omit(res, 'error');
    }
  } catch (e) {
    error = e;
  }

  return this.callback(error, args);
};

module.exports = Task;