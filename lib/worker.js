/**
 * Created by dzambuto on 27/09/15.
 */
var debug = require('debug')('micro:task');

var _ = require('underscore')
  , vm = require('vm')
  , net = require('net')
  , path = require('path')
  , BufferStream = require('bufferstream');

/**
 * Worker class
 * @param options
 * @constructor
 */

function Worker (options) {
  options = options || {};

  _.extend(this, {
      socket: '/tmp/worker.sock',
      delay: 500,
      timeout: 500,
      separator: '\u0000\u0000',
      cwd: process.cwd()
    }, options);

  if (this.apiPath) {
    this.api = require(this.apiPath).api;
  }
}

/**
 * Start listening incoming connection
 */
Worker.prototype.listen = function () {
  var worker = this;

  function restart (err) {
    setTimeout(function () {
      debug('restarting from error: %s', err.toString());
      worker.listen();
    }, this.delay);
  }

  this.server = net.createServer(function (connection) {
    var stream = new BufferStream({ size:'flexible' });

    stream.split(worker.separator, function (data) {
      worker.executeTask(connection, data);
    });

    connection.on('data', function (data) {
      stream.write(data.toString())
    });

    debug('worker started');
  });

  this.server.listen(this.socket, function() {
    console.log('__worker__started__');
  });

  this.server.on('error', restart);

  process.on('uncaughtException', function (err) {
    var err = { message: err.message, stack: err.stack };
    console.log(JSON.stringify(err));
  });
};

/**
 * Execute untrusted code in a safe runner
 * @param connection
 * @param data
 */
Worker.prototype.executeTask = function (connection, data) {
  var worker = this
    , context
    , apiModule
    , log = []
    , timer;

  var global = {
    exit: function (err, args) {
      clearTimeout(timer);

      if (err)
        return worker.sendError(connection, err);

      connection.write(JSON.stringify({ context: args, console: log }) + worker.separator);
    },
    publish: function (event, message) {
      "use strict";

      switch (event) {
        case 'stdout':
          log.push(JSON.parse(message)[0]);
          break;
        default:
          throw new Error('Unknown event type');
      }
    }
  };

  if ((apiModule = this.api)) {
    Object
      .keys(apiModule)
      .forEach(function (key) {
        global[key] = apiModule[key];
      });
  }

  try {
    task = JSON.parse(data);

    global.code = task.code;
    global.args = task.args;

    timer = setTimeout(function () {
      clearTimeout(timer);
      worker.sendError(connection, new Error('Script execution timed out.'));
    }, this.timeout + 100);

    context = vm.createContext(global);

    vm.runInContext(
      '(' + getSafeRunner.toString() + ')()',
      context,
      { timeout: this.timeout }
    );

  } catch (e) {
    debug('safe runner error %s', e);

    if (timer)
      clearTimeout(timer);

    worker.sendError(connection, e);
  }
};

/**
 * Send an error to client (Task)
 * @param connection
 * @param err
 */
Worker.prototype.sendError = function (connection, err, log) {
  var error = {
      'message': err.message,
      'stack': err.stack
    }
    , result = { error: error };

  if (log)
    result.console = log;

  connection.write(JSON.stringify(result) + this.separator);
};

/**
 * Generate a safe runner environment
 * @returns {Function}
 */
function getSafeRunner () {
  var global = this;

  function UserScript(str) {
    var factory, clientFn;

    str = str + '';

    try {
      factory = new Function('return ' + str);
      clientFn = factory();

      if (typeof clientFn !== 'function') {
        throw new Error('The code does not return a JavaScript function.');
      }

      if (clientFn.length !== 2) {
        throw new Error('The JavaScript function must have one of the following signature: (context, cb)');
      }
    } catch (e) {
      throw new Error('Unable to compile submitted JavaScript. ' + e.message);
    }

    return clientFn;
  }

  return (function start(code, obj) {
    "use strict";

    var send = function (event, message) {
      publish(event, JSON.stringify([].slice.call(arguments,1)));
    };

    global.console = {
      log: send.bind(global, 'stdout')
    };

    UserScript(code)(obj, exit);
  })(code, args)
}

module.exports = Worker;