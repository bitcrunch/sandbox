/**
 * Created by dzambuto on 28/09/15.
 */

var debug = require('debug')('micro:task');

var _ = require('underscore')
  , fs = require('fs')
  , os = require('os')
  , util = require('util')
  , spawn = require('child_process').spawn
  , EventEmitter = require('events').EventEmitter
  , Task = require('./task');

var SIGHUP = os.platform() === 'win32' ? 'SIGTERM' : 'SIGHUP';

function Factory (options) {
  var factory = this;

  options = options || {};

  EventEmitter.call(this);

  _.extend(this, {
    node: process.execPath,
    timeout: 2000,
    socket: '/tmp/worker.sock',
    workerScript: __dirname + '/../bin/start.js',
    memoryLimitMB: 0,
    apiPath: ''
  }, options);

  this.isInitialized = false;
  this.isWaiting = false;
  this.lastHeartbeat = (new Date()).getTime();

  this.spawnWorker();
  this.startHeartbeat();

  process.on('exit', function() {
    factory.worker.kill(SIGHUP);
  });
}

util.inherits(Factory, EventEmitter);

Factory.prototype.ready = function (fn) {
  var factory = this;

  if (this.isInitialized) {
    return fn();
  }

  setTimeout(function () {
    factory.ready(fn);
  }, 500);
};

Factory.prototype.close = function () {
  debug('closing connection');
  clearInterval(this.timer);
  this.worker.removeAllListeners('exit');
  this.worker.kill(SIGHUP);
  process.removeAllListeners('exit');
};

Factory.prototype.spawnWorker = function () {
  var factory = this;

  debug('spawning worker');

  try { fs.unlinkSync(this.socket); } catch (e) {}

  this.worker = spawn(this.node, [
    "--max_old_space_size=" + this.memoryLimitMB.toString(),
    this.workerScript,
    '--socket=' + this.socket,
    '--timeout=' + this.timeout,
    '--api=' + this.apiPath
  ], { env: { 'DEBUG': 'micro:*' }, stdio: ['pipe', 'pipe', 'pipe', 'ipc'] });

  this.worker.stdout.on('data', function(data) {
    var str = data.toString()
      , err;

    debug('worker sent:' + str);

    if (str === '__worker__started__\n') {
      factory.isWaiting = false;
      factory.isInitialized = true;
    }
  });

  this.worker.on('error', function (err) {
    debug('worker exception: %s', err);
  });

  this.worker.on('exit', function (code) {
    debug('worker exited');
    factory.emit('exit');
    factory.spawnWorker();
  });
};

Factory.prototype.startHeartbeat = function () {
  var factory = this;

  debug('starting heartbeat')

  this.timer = setInterval(function () {
    var now = (new Date()).getTime();

    if (!factory.isWaiting) {
      factory.isWaiting = true;
      factory
        .createTask('function (ctx, done) { return done (null, true); }')
        .run(null, function (err, res) {
          factory.isWaiting = false;
          factory.lastHeartbeat = (new Date()).getTime();
        });
    }

    if ( (now - factory.lastHeartbeat) > factory.timeout) {
      factory.lastHeartbeat = (new Date()).getTime();

      if (factory.isInitialized) {
        debug('killing worker');
        factory.isInitialized = false;
        factory.worker.kill(SIGHUP);
      }
    }
  }, 500);
};

Factory.prototype.createTask = function (code, options) {
  options = options || {};

  _.defaults(options, {
    factory: this,
    timeout: 500,
    socket: this.socket
  });

  return new Task(code, options);
};

module.exports = Factory;