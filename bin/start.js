/**
 * Created by dzambuto on 28/09/15.
 */

var program = require('commander');
var Worker = require('../lib/worker');
var version = require('../package.json').version || '0.1.0';
var worker, options = {};

program
  .version(version)
  .option('-t, --timeout [value]', 'timeout')
  .option('-s, --socket [value]', 'socket path')
  .option('-a, --api [value]', 'api path')
  .parse(process.argv);

if (program.timeout)
  options.timeout = parseInt(program.timeout);

if (program.socket)
  options.socket = program.socket;

if (program.api)
  options.apiPath = program.api;

worker = new Worker(options);
worker.listen();